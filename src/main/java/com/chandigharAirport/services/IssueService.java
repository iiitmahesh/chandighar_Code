package com.chandigharAirport.services;

import java.util.List;

import com.chandigharAirport.web.model.Issue;
import com.chandigharAirport.web.model.SearchCriteria;

public interface IssueService {


	Issue findById(int id);

	void saveIssue(Issue issue);

	void updateIssue(Issue issue);

	void deleteIssueById(int ssn);

	List<Issue> findAllIssues();

	Issue findIssueById(int ssn);

	List<Issue> findIssue(Issue issue);

	List<Issue> findIssue(SearchCriteria searchCriterea);

}
