package com.chandigharAirport.services;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.chandigharAirport.dao.UserDao;
import com.chandigharAirport.web.model.User;

 
 
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {
 
    @Autowired
    private UserDao dao;
    
	@Autowired
	MessageSource messageSource;

     
    public User findById(int id) {
        return dao.findById(id);
    }
 
    public void saveUser(User User) {
        dao.saveUser(User);
    }
 
    /*
     * Since the method is running with Transaction, No need to call hibernate update explicitly.
     * Just fetch the entity from db and update it with proper values within transaction.
     * It will be updated in db once transaction ends. 
     */
    public void updateUser(User user) {
//        User entity = dao.findById(User.getId());
        User entity = dao.findUserByUsername(user.getUsername());
        System.out.println(" to be udpated user s:"+entity);
        if(entity!=null){
            entity.setUsername(user.getUsername());
            entity.setPassword(user.getPassword());
            entity.setEmail(user.getEmail());
            entity.setPhone(user.getPhone());
            entity.setAddress(user.getAddress());
        }
dao.updateUser(entity);        
        
    }
 
    public void deleteUserBySsn(String ssn) {
        dao.deleteUserByUsername(ssn);
    }
     
    public List<User> findAllUsers() {
        return dao.findAllUsers();
    }
 
    public User findUserBySsn(String ssn) {
        return dao.findUserByUsername(ssn);
    }
 
    public boolean isUserSsnUnique(Integer id, String ssn) {
        User User = findUserBySsn(ssn);
        return ( User == null || ((id != null) && (User.getId() == id)));
    }

	public List<User> searchUsers(User user) {
		// TODO Auto-generated method stub
		return dao.searchUsers(user);
	}

	public List<User> searchUsersByUsername(User user) {
		// TODO Auto-generated method stub
		return dao.searchUsersByUsername(user.getUsername());
	}

	public User searchUsersAuth(User user) {
		// TODO Auto-generated method stub
		return dao.searchUsersAuth(user.getUsername() , user.getPassword());
	}
     
}