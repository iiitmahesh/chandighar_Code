package com.chandigharAirport.web.model;

public class SearchCriteria {

	String username;
	String email;
	String status;
	String department;
	
	int lastUpdatedIn;
	int createdMoreThan  = -1 ;

	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public int getLastUpdatedIn() {
		return lastUpdatedIn;
	}

	public void setLastUpdatedIn(int lastUpdatedIn) {
		this.lastUpdatedIn = lastUpdatedIn;
	}

	public int getCreatedMoreThan() {
		return createdMoreThan;
	}

	public void setCreatedMoreThan(int createdMoreThan) {
		this.createdMoreThan = createdMoreThan;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "SearchCriteria [username=" + username + ", email=" + email
				+ ", status=" + status + ", department=" + department
				+ ", lastUpdatedIn=" + lastUpdatedIn + ", createdMoreThan="
				+ createdMoreThan + "]";
	}
	
	

}
