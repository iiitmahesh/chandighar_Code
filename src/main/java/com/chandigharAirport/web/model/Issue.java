package com.chandigharAirport.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.chandigharAirport.web.jsonview.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "ISSUE")
public class Issue {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@JsonView(Views.Public.class)
	private int id;

    @Column(name = "NAME", nullable = false)
    @JsonView(Views.Public.class)
    private String name;

    @NotNull
    @DateTimeFormat(pattern="dd/MM/yyyy HH:mm:ss") 
    @Column(name = "CREATE_TIME", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    @JsonView(Views.Public.class)
    private LocalDateTime createTime;
    
    @NotNull
    @DateTimeFormat(pattern="dd/MM/yyyy HH:mm:ss") 
    @Column(name = "UPDATE_TIME", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    @JsonView(Views.Public.class)
    private LocalDateTime updateTime;
    
    
    
    @Column(name = "STATUS", nullable = false)
	@JsonView(Views.Public.class)
    private String status;
        
    
    @Column(name = "DESCRIPTION", nullable = false)
    @JsonView(Views.Public.class)
    private String description;
    
    @Column(name = "CREATED_BY", nullable = false)
    @JsonView(Views.Public.class)
    private int createdBy;
    
    @Column(name = "DEPARTMENT", nullable = false)
    @JsonView(Views.Public.class)
    private String department;
    
    @Column(name = "IMAGE", nullable = false)
    @JsonView(Views.Public.class)
    private String image;
    
	
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}


	public LocalDateTime getCreateTime() {
		return createTime;
	}

	public void setCreateTime(LocalDateTime createTime) {
		this.createTime = createTime;
	}

	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(int createdBy) {
		this.createdBy = createdBy;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Issue))
            return false;
        Issue other = (Issue) obj;
        if (id != other.id)
            return false;
        return true;
    }

	@Override
	public String toString() {
		return "Issue [id=" + id + ", name=" + name + ", createTime=" + createTime + ", status=" + status
				+ ", description=" + description + ", createdBy=" + createdBy + ", department=" + department
				+ ", image=" + image + "]";
	}


}
