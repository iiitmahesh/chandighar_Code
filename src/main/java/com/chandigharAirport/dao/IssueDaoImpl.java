package com.chandigharAirport.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.springframework.stereotype.Repository;

import com.chandigharAirport.web.model.Issue;
import com.chandigharAirport.web.model.SearchCriteria;

@Repository("IssueDao")
public class IssueDaoImpl extends AbstractDao<Integer, Issue>implements IssueDao {

	public Issue findById(int id) {
		return getByKey(id);
	}

	public void saveIssue(Issue Issue) {
		persist(Issue);
	}

	public void updateIssue(Issue Issue) {
		update(Issue);
	}

	public void deleteIssueById(int issueId) {
		Query query = getSession().createSQLQuery("delete from Issue where id = :id");
		query.setInteger("Id", issueId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<Issue> findAllIssues() {
		Criteria criteria = createEntityCriteria();
		return (List<Issue>) criteria.list();
	}

	public List<Issue> findIssue(Issue issue) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();

		if (issue.getStatus() != null) {
			criteria.add(Restrictions.eq("status", issue.getStatus()));
		}

		if (issue.getDepartment() != null) {
			criteria.add(Restrictions.eq("department", issue.getDepartment()));
		}

		return (List<Issue>) criteria.list();

	}

	public List<Issue> findIssue(SearchCriteria searchCriterea) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();

		if (searchCriterea.getCreatedMoreThan() != -1) {
			int createdMoreThan = searchCriterea.getCreatedMoreThan();
			
//			DateTime hoursBefore = new DateTime();
			LocalDateTime hoursBefore = new LocalDateTime();
//			Calendar hoursBefore = Calendar.getInstance();
//			hoursBefore.add(Calendar.HOUR, -createdMoreThan);
			criteria.add(Restrictions.between("createTime", hoursBefore.minusHours(createdMoreThan), hoursBefore));
		}
		
		if (searchCriterea.getStatus() != null) {
			criteria.add(Restrictions.eq("status", searchCriterea.getStatus()));
		}

		if (searchCriterea.getDepartment() != null) {
			criteria.add(Restrictions.eq("department", searchCriterea.getDepartment()));
		}

		

		return (List<Issue>) criteria.list();
	}

}