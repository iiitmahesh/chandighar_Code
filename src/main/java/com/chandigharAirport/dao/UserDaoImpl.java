package com.chandigharAirport.dao;

import java.lang.reflect.Field;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.chandigharAirport.web.model.User;
 
 
@Repository("UserDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {
 
    public User findById(int id) {
        return getByKey(id);
    }
 
    public void saveUser(User User) {
        persist(User);
    }
    
    public void updateUser(User User) {
    	update(User);
    }
 
    public void deleteUserByUsername(String username) {
        Query query = getSession().createSQLQuery("delete from User where Username = :username");
        query.setString("username", username);
        query.executeUpdate();
    }
 
    @SuppressWarnings("unchecked")
    public List<User> findAllUsers() {
        Criteria criteria = createEntityCriteria();
        return (List<User>) criteria.list();
    }
 
    public User findUserByUsername(String username) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("username", username));
        return (User) criteria.uniqueResult();
    }

	public List<User> searchUsers(User user) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		criteria = buildCritereaOnAllObject(criteria , user );
		return (List<User>) criteria.list();
	}

	private Criteria buildCritereaOnAllObject(Criteria criteria  , User user){
		if(user.getUsername()!=null){
			criteria.add(Restrictions.ilike("username", user.getUsername()));	
		}
		if(user.getEmail()!=null){
			criteria.add(Restrictions.ilike("email", user.getEmail()));	
		}
		if(user.getName()!=null){
			criteria.add(Restrictions.ilike("name", user.getName()));	
		}
		if(user.getAddress()!=null){
			criteria.add(Restrictions.ilike("address", user.getAddress()));	
		}
	    return criteria;
	}
	
	public List<User> searchUsersByUsername(String username) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.ilike("username", username));
        return (List<User>) criteria.list();
	}

	public User searchUsersAuth(String username, String password) {
	       Criteria criteria = createEntityCriteria();
	        criteria.add(Restrictions.eq("username", username));
	        criteria.add(Restrictions.eq("password", password));
	        return (User) criteria.uniqueResult();
	}

}