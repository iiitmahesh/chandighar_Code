'use strict';
/**
 * Components that are must 
 * 
 * 
 */
angular.module('myApp.appAdminService', ['ngRoute']).service('appAdminService', ['$http', function($http) {

    var cudAppurl = centralServer + '/cudApp'
    var cudRolesurl = centralServer + '/cudRoles'
    var cudSubpageIntrourl = centralServer + '/cudSubPageIntro'
    var cudSubpageFullurl = centralServer + '/cudSubPageFull'
    var getFullDetails = centralServer + '/getFullDetails'
    var bookmarkAppURL = centralServer + '/bookmarkApp'
    var unBookmarkURL = centralServer + '/unBookmark'
    var createUserURL = centralServer + '/createUser'
    var sendNotificationURL = centralServer + '/sendNotification'
    var sendNotificationPhysicalAppsURL = centralServer + '/sendNotificationPhysicalApps'
    var validateLoginURL = centralServer + '/validateLogin'
    var viewAppFromUsersCopyURL = centralServer + '/viewAppFromUsersCopy'
    var deleteAppURL = centralServer + '/deleteApp'
    var deleteUserNoteURL = centralServer + '/deleteUserNotification'
    var submitRegistrationResponseURL = centralServer + '/submitRegistrationResponse'
    var emailCreatorAllResponsesURL = centralServer + '/emailCreatorAllResponses'
    var uploadImagesURL = centralServer + '/uploadImages'
    var deleteImageFromSpaceURL = centralServer + '/deleteImageFromSpace'
    var getIntroURL = centralServer + '/getIntro'
    var requestAppInstallationURL = centralServer + '/requestAppInstallation'
    var getAppRequestsURL = centralServer + '/getAppRequests'
    var updateUserAppAccessURL = centralServer + '/updateUserAppAccess'
    var respondToAdminURL = centralServer + '/respondToAdmin'
    var getEmptyProdDetailsPageURL = centralServer + '/getEmptyProdDetailsPage'
    var updateProductDetailsURL = centralServer + '/updateProductDetails'
    var loadProductDetailsURL = centralServer + '/loadProductDetails'
    var generateAPKURL = centralServer + '/generateAPK'
    var generateWFolderURL = centralServer + '/generateWFolder'
    var getTemplatesSupportedURL = centralServer + '/getTemplatesSupported'
    var updateUserProfileURL = centralServer + '/updateUserProfile'
    var getRolesURL = centralServer + '/getRoles'
    var provideAccessURL = centralServer + '/provideAccess'
    var logoutURL = centralServer + '/logout'

    var centralServerDomain = centralServer.substring(0, centralServer.indexOf('api') - 1);


    var regUserForNtfnsPhysicalAppURL = centralServerDomain + '/regUserForNtfnsPhysicalApp'


    /**
     @detail JSON must contain values -> userIntroOid(:string),appIntroOid(:string),appJSON(:object),cudCommand(:int)2/3 for create do not send appId a new one will get created
     * @param {type} app
     * @returns {unresolved}
     */
    this.saveApp = function(app) {
        console.log(" in saveApp service3" + JSON.stringify(app));
        return $http.post(cudAppurl, app);
    }


    this.provideAccess = function(app) {
        console.log(" in saveApp service3" + JSON.stringify(app));
        return $http.post(provideAccessURL, app);
    }

    this.logout = function() {
        console.log(" in logout service ");
        $http.post(logoutURL).then(function successCallback(response) {
            console.log("Success with response : " + JSON.stringify(response.data));
            window.location.href = "/";
        }, function errorCallback(response) {
            console.log("error : :( ")
        });
    }



    this.savePushNotificationRegId = function(userOid, regId, pageOid) {

        console.log(" in saveApp savePushNotificationRegId " + regUserForNtfnsPhysicalAppURL + "/" + userOid + "/" + regId + "/" + pageOid);

        // alert(" in saveApp savePushNotificationRegId "+regUserForNtfnsPhysicalAppURL + "/" + userOid + "/" + regId.length+"/"+pageOid)


        $http({
            method: 'GET',
            url: regUserForNtfnsPhysicalAppURL + "/" + userOid + "/" + regId + "/" + pageOid
        }).then(function successCallback(response) {
            // this callback will be called asynchronously
            // when the response is available
            console.log(" registration id sent to server  ")
                // alert(" registration id sent to server  "+JSON.stringify(response))
        }, function errorCallback(response) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            console.log(" registration id sendn to server  failed . response:" + JSON.stringify(response))
            console.log(" registration id send failed, you might not recieve push notifications.")
                //alert(" registration id send failed, you might not recieve push notifications.")
        });

        // var toServer = {userOid:userOid , pageOid:pageOid , message:message}


    }


    this.cudRoles = function(app) {
        console.log(" in cudroles service3" + JSON.stringify(app));
        return $http.post(cudRolesurl, app);
    }

    this.updateUserAppAccess = function(app) {
        console.log(" in saveApp service3");
        return $http.post(updateUserAppAccessURL, app);
    }

    this.getEmptyProdDetailsPage = function(app) {
        console.log(" in getEmptyProdDetailsPage");
        return $http.post(getEmptyProdDetailsPageURL, app);
    }

    this.generateAPK = function(app) {
        console.log(" in generateAPK " + app);
        return $http.post(generateAPKURL, app);
    }

    this.generateWFolder = function(app) {
        console.log(" in generateAPK " + app);
        return $http.post(generateWFolderURL, app);
    }



    this.respondToAdmin = function(userOid, pageOid, message) {
        console.log(" in saveApp service3");
        var toServer = {
            userOid: userOid,
            pageOid: pageOid,
            message: message
        }
        return $http.post(respondToAdminURL, app);
    }




    /**
     @detail JSON must contain values -> userIntroOid(:string),appIntroOid(:string),appJSON(:object),cudCommand(:int)2/3 for create do not send appId a new one will get created
     * @param {type} app
     * @returns {unresolved}
     */
    this.getIntro = function(introOId) {

        var jsonToServer = {
            introOID: introOId
        };

        console.log(" in saveApp loadAppIntro.");
        return $http.post(getIntroURL, jsonToServer);
    }


    this.getAppRequests = function(introOId) {

        var jsonToServer = {
            userOID: introOId
        };

        console.log(" in saveApp loadAppIntro.");
        return $http.post(getAppRequestsURL, jsonToServer);
    }




    this.deleteImageFromSpace = function(app) {
        console.log(" in saveApp service3");
        return $http.post(deleteImageFromSpaceURL, app);
    }




    // !! Assumes variable fileURL contains a valid URL to a text file on the device,
    //    for example, cdvfile://localhost/persistent/path/to/file.txt






    this.sendResponseToOwnerMail = function(app) {
        return $http.post(emailCreatorAllResponsesURL, app);
    }



    this.unBookmarkApp = function(app) {

        return $http.post(unBookmarkURL, app);

    }


    this.deleteApp = function(app) {

        return $http.post(deleteAppURL, app);

    }





    this.viewAppFromUsersCopy = function(app) {
        return $http.post(viewAppFromUsersCopyURL, app);
    }



    /**Need to test if this is working.
     * 
     * @param {type} userId
     * @param {type} password
     * @param {type} loc
     * @returns {undefined}
     */
    this.validateLogin = function(userId, password, loc) {
        console.log(" in saveApp service3");
        var jsonData = {};
        jsonData.userId = userId;
        jsonData.password = password;
        console.log(JSON.stringify(jsonData))
        $http.post(validateLoginURL, jsonData).then(function successCallback(response) {

            console.log("Success with response : " + JSON.stringify(response.data));
            if (response.data.indexOf("#") == -1) {
                alert(response.data)
                return;
            } else {
                loc.path('/home');
            }

        }, function errorCallback(response) {
            console.log(" response : " + JSON.stringify(response));

        });
    }



    this.saveSubPages = function(appid, subPageIntro) {

        var subPageJSOn = {};
        console.log(" subPageIntro " + JSON.stringify(subPageIntro));
        subPageJSOn.pageJSON = subPageIntro;
        subPageJSOn.appOID = appid;

        return $http.post(cudSubpageIntrourl, subPageJSOn);



    }


    this.savePageDetails = function(pageIntroOid, textPageData) {

        var subPageJSOn = {};

        subPageJSOn.pageJSON = textPageData;
        subPageJSOn.pageIntroOid = pageIntroOid;
        console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));
        return $http.post(cudSubpageFullurl, subPageJSOn);
    }


    this.updateProductDetails = function(pageIntroOid, pageData) {
        var subPageJSOn = {};
        subPageJSOn.pageJSON = pageData;
        subPageJSOn.pageIntroOid = pageIntroOid;
        console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));
        return $http.post(updateProductDetailsURL, subPageJSOn);
    }

    this.submitRegistrationResponse = function(userinputs, pageOid, reponseOid) {
        // console.log(" in submitRegistrationResponse  userinputs : " + JSON.stringify(userinputs)+" pageOid "+pageOid+" reponseOid "+reponseOid);

        var subPageJSOn = {};

        subPageJSOn.userInputs = {
            values: userinputs
        };


        if (userINTROOID == null) {
            userINTROOID = "#13:0";
        }
        

        subPageJSOn.pageIntroOid = pageOid;
        subPageJSOn.userOID = userINTROOID;
        subPageJSOn.reponseOid = reponseOid;

        // console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));
        // console.log(" subPageJSOn.pageIntroOid " + JSON.stringify(subPageJSOn));
        console.log(" userINTROOID  " + JSON.stringify(userINTROOID));
        
        console.log(" subPageJSOn.reponseOid " + JSON.stringify(subPageJSOn.reponseOid));
        return $http.post(submitRegistrationResponseURL, subPageJSOn);
    }


    this.loadPageDetails = function(pageIntroOid, pageData) {

        var subPageJSOn = {};

        subPageJSOn.introOID = pageIntroOid;
        console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));
        //            console.log(" cudSubpageFullurl " + cudSubpageFullurl);

        $http.post(getFullDetails, subPageJSOn).then(function successCallback(response) {
            console.log("Success with response : " + JSON.stringify(response.data));
            if (response.data == "error") {
                alert(" Sorry some internal error has occured. ")
                return;
            }

            pageData = response.data;
            return pageData;
        }, function errorCallback(response) {

            console.log(" response : " + JSON.stringify(response));
            return pageData;
        });
    }

    this.loadPageDetailsV2 = function(pageIntroOid, pageData) {

        var subPageJSOn = {};

        subPageJSOn.introOID = pageIntroOid;
        console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));
        //            console.log(" cudSubpageFullurl " + cudSubpageFullurl);

        return $http.post(getFullDetails, subPageJSOn);

    }

    this.loadProductDetails = function(pageIntroOid) {
        var subPageJSOn = {};
        subPageJSOn.introOID = pageIntroOid;
        console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));
        return $http.post(loadProductDetailsURL, subPageJSOn);
    }





    this.bookmarkApp = function(userOID, appOID) {
        var toServer = {};
        toServer.userOID = userOID;
        toServer.appOID = appOID;
        console.log(" subPageJSOn " + JSON.stringify(toServer));
        return $http.post(bookmarkAppURL, toServer);
    }


    this.requestAppInstallation = function(userOID, appOID) {
        var toServer = {};
        toServer.userOID = userOID;
        toServer.appOID = appOID;
        console.log(" subPageJSOn " + JSON.stringify(toServer));
        return $http.post(requestAppInstallationURL, toServer);
    }


    this.register = function(user) {
        console.log("createUserURL"+createUserURL+":user"+JSON.stringify(user))

        // alert("  made register call createUserURL "+JSON.stringify(user));


        $http.post(createUserURL, user).then(function successCallback(response) {
            console.log("Success with response : " + JSON.stringify(response.data));
            if (response.data == "error") {
                alert(" Sorry some internal error has occured. ")
                return;
            }

            userINTROOID = user.rid = response.data;
            saveData(userINTROOID_KEY, user.rid);


        }, function errorCallback(response) {
            console.log("server error.res:"+response)

        });

    }

    this.updateUserProfile = function(user) {
        return $http.post(updateUserProfileURL, user)
    }


    this.deleteUserNotification = function(note) {

        return $http.post(deleteUserNoteURL, note);

    }



    this.sendNotificationPhysicalApps = function(note) {
        return $http.post(sendNotificationPhysicalAppsURL, note);
    }

    this.sendNotification = function(note) {
        return $http.post(sendNotificationURL, note);
    }

    this.getTemplatesSupported = function(note) {
        return $http.post(getTemplatesSupportedURL, note);
    }

    this.getRoles = function() {

        return $http.post(getRolesURL);

    }



}]);
