angular.module('starter', ['ngRoute',
    'ionic', 'ngCordova','ionic.native',
    'starter.controllers', 'myApp.registration-WBG', 'myApp.list-thumbSquareLeftWBG', 'myApp.home-rowListFull', 'myApp.menu-iconLeft', 'myApp.login-buttons', // Services 
    'myApp.appAdminService',
    'myApp.socialNetworkService',
    'myApp.statsUpdateService',
    'myApp.authService',
    'gamifad',
    'gamifad.services'


])

.run(function($ionicPlatform, $ionicPopup, $cordovaPushV5, $http, $rootScope, appAdminService, statsUpdateService) {


    document.addEventListener("deviceready", function() {

// PLACE PUSH NOTIFICATION DATA 
        // initialize push notifications here 
        // command : cordova plugin add phonegap-plugin-push --variable SENDER_ID="601672273692"
        var options = {
            android: {
                senderID: "601672273692"
            },
            ios: {
                alert: "true",
                badge: "true",
                sound: "true"
            },
            windows: {}
        };


        // alert(" about to init push notes, doc is ready .")  
        try {


            // initialize
            $cordovaPushV5.initialize(options).then(function() {


                // start listening for new notifications
                $cordovaPushV5.onNotification();

                // start listening for errors
                $cordovaPushV5.onError();


                // register to get registrationId
                $cordovaPushV5.register().then(function(registrationId) {
                    // save `registrationId` somewhere;



                    regId_M = registrationId;
                    // alert("  registrationId "+registrationId)

                })



            });


            // triggered every time notification received
            $rootScope.$on('$cordovaPushV5:notificationReceived', function(event, data) {
                // data.message,
                // data.title,
                // data.count,
                // data.sound,
                // data.image,
                // data.additionalData
                alert("New activity in AMS app.");
            });


            // alert("  $cordovaPushV5:errorOcurred GETTING READY  ")
            // triggered every time error occurs
            $rootScope.$on('$cordovaPushV5:errorOcurred', function(event, e) {
                // e.message
                // alert("  $cordovaPushV5:errorOcurred  "+JSON.stringify(e)+"\n msg : "+e.message+"\n : "+JSON.stringify(event))

            });


        } catch (err) {
            alert(" eror "+JSON.stringify(err)+"err"+err)
        }

// alert(" push  init completed ")

    });




    $ionicPlatform.ready(function() {
        // initMethodsList();

// INITIALIZATIN STUFF GOES HERE
console.log(" about to go to - appjs - in init ...!!! 1");


        console.log("  on ready called userINTROOID " + userINTROOID + " globalPageIntroOid : " + globalPageIntroOid);
        // appAdminService.savePushNotificationRegId( escape(userINTROOID) , escape("Dummy regId for testing only ") , escape(globalPageIntroOid) );
        // appAdminService.testMeth();
        // appAdminService.saveApp({});
        

        var getFullDetails = centralServer + '/getIntro'


        var subPageJSOn = {};
        userINTROOID = getSavedData(userINTROOID_KEY)
        /*if(userINTROOID == undefined){
            userINTROOID = "#13:53";
        }
*/

        subPageJSOn.introOID = userINTROOID;
        // console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));

// alert(" init step completed . "+JSON.stringify(subPageJSOn));


 // IF CONDITION WITH HHTP REQUEST GOES HERE 
         if (userINTROOID != undefined && userINTROOID != null && userINTROOID != "") {


            $http.post(getFullDetails, subPageJSOn).then(function successCallback(response) {
                // console.log("Success with response : " + JSON.stringify(response.data));
                if (response.data == "error") {
                    window.alert("Could not load user details , please check your internet connection.");
                    // statsUpdateService.log("New Tab Created " + " from page : " + appPage);
                    return;
                }

                userDetails = response.data;
                userRoles = response.data.roles;
                // console.log("  dry test response.data "+JSON.stringify(response.data))
                // console.log("  dry test "+userINTROOID+" "+globalPageIntroOid+"  userRoles "+userRoles)

                appAdminService.savePushNotificationRegId(escape(userINTROOID), escape(regId_M), escape(globalPageIntroOid));

                appAdminService.bookmarkApp(userINTROOID, globalPageIntroOid).then(function successCallback(response) {
                    // console.log("Success with response : " + JSON.stringify(response.data));
                    // console.log("Success with response : " + response.data);
                    if (response.data == "error") {
                        alert(" Sorry some internal error has occured. ")
                        return;
                    }
                    // console.log("modified :  response : " + JSON.stringify(response.data));
                    // alert(" bookmark completed ");

                }, function errorCallback(response) {
                    console.log(" response : " + JSON.stringify(response));
                    // alert(" Sorry , unable to bookmark  ");
                    return pageData;
                });

            }, function errorCallback(response) {

                console.log(" response : " + JSON.stringify(response));
                // statsUpdateService.log("user callback as failed during loadDetails of textTab   : with error as :  " + JSON.stringify(response))
                window.alert("Could not load user details , please check your internet connection. ");

            });



console.log(" in init ...!!! IN IF  1");


        }



// alert(" out of init ...!!! IN IF  1");


        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
// KEYBOARD CODE TO BE PLACED HERE

        if (window.cordova && window.cordova.plugins.Keyboard) {
            try{

             cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
             cordova.plugins.Keyboard.disableScroll(true);
            // alert("initialize done. window.cordova "+window.cordova+"  window.cordova.plugins.Keyboard "+window.cordova.plugins.Keyboard);
            }catch(err){
                alert(" error "+err)
            }

        }

        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }

// alert(" keyboard disabled . ") 

        deviceInformation = JSON.stringify(ionic.Platform.device());

console.log(" device info taken in ...!!! IN IF  1");
// INTERNET AVAILABILITY CHECK HERE 

        if (window.Connection) {
            if (navigator.connection.type == Connection.NONE) {
                $ionicPopup.confirm({
                        title: 'No Internet Connection',
                        content: 'Sorry, no Internet connectivity detected. Please reconnect and try again.'
                    })
                    .then(function(result) {
                        if (!result) {
                            ionic.Platform.exitApp();
                        }
                    });
            }
        }

// alert(" device connection validated  ...!!! IN IF  1");


    });
})

.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

        .state('app', {
            url: '/app',
            abstract: true,
            templateUrl: 'screens/menu-iconLeft/menu.html',
            controller: 'menuCtrl'
        })
        .state('app.registration-WBG', {
            url: '/registration-WBG/:pageOid/:appIntroOid',
            cache: false,
            views: {
                'menuContent': {
                    templateUrl: 'screens/registration-WBG/registration.html',
                    controller: 'registrationCtrl'
                }
            }
        })
        .state('app.list-thumbSquareLeftWBG', {
            url: '/list-thumbSquareLeftWBG/:pageOid/:filterCriterea',
            views: {
                'menuContent': {
                    templateUrl: 'screens/list-thumbCircleLeftWBG/list.html',
                    controller: 'listCtrl'
                }
            }
        })
        .state('app.home-rowListFull', {
            url: '/home-rowListFull',
            views: {
                'menuContent': {
                    templateUrl: 'screens/home-rowListFull/home.html',
                    controller: 'homeCtrl'
                }
            }
        })
        .state('app.login-buttons', {
            url: '/login-buttons',
            views: {
                'menuContent': {
                    templateUrl: 'screens/login-buttons/login-buttons.html',
                    controller: 'loginCtrl'
                }
            }
        })
        // $urlRouterProvider.otherwise('/app/home-rowListFull');
    $urlRouterProvider.otherwise('/login-buttons');

});
