'use strict';

angular.module('myApp.list-thumbSquareLeftWBG', ['ngRoute'])

// .config(['$routeProvider', function($routeProvider) {
//     $routeProvider.when('/list-thumbCircleLeftWBG/:pageOid', {
//         templateUrl: 'screens/list-thumbCircleLeftWBG/list.html',
//         controller: 'listCtrl'
//     });
// }])

.controller(
    'listCtrl', [
        '$scope',
        '$state',
        '$location',
        '$stateParams',
        '$http',
        '$ionicPopup',
        '$routeParams',
        '$timeout',
        '$ionicModal',
        'appAdminService',
        'statsUpdateService',
        function($scope, $state, $location, $stateParams, $http, $ionicPopup, $routeParams, $timeout, $ionicModal, appAdminService, statsUpdateService) {

            // var pageIntroOid = $routeParams.pageOid;
            var pageIntroOid = $stateParams.pageOid;
            var filterCritereaFromState = $stateParams.filterCriterea;
            var filterCriterea = currentPageDetails.filterCriterea;
            var tempLOD = [];
            var alertPopupV;


            if (filterCriterea == null || filterCriterea == undefined) {

                if (filterCritereaFromState != null || filterCritereaFromState != undefined) {
                    filterCriterea = filterCritereaFromState;
                }

            }




            console.log(" pageIntroOid in view " + pageIntroOid + " filterCriterea " + filterCriterea);



            var getFullDetails = centralServer + '/getFullDetails';
            $scope.centralServer = centralServer;
            $scope.centralServer1 = centralServer1;

            var appPage = "list";
            var listOfData = [];
            var citem = {};
            citem.iHeader = "";

            var descList = [];
            var desc = {};
            desc.label = "";
            desc.value = "";
            descList.push(desc);
            citem.descList = descList;
            listOfData.push(citem);

            $scope.listOfData = listOfData;
            var toServer = {};
            $scope.internalVars = { deptnames: [] };
            $scope.internalVars.showFilters = false;

            $scope.internalVars.deptnames.push("Electrical");
            $scope.internalVars.deptnames.push("Civil");
            $scope.internalVars.deptnames.push("Operations");
            $scope.internalVars.deptnames.push("Airport System");
            $scope.internalVars.deptnames.push("Facilities");
            $scope.internalVars.deptnames.push("CNS IT");
            $scope.internalVars.deptnames.push("L and T");
            $scope.internalVars.deptnames.push("Housekeeping");
            $scope.internalVars.filedInputValue = "";





            if ("notYetSaved" == pageIntroOid) {
                // alert(" There is some error with this page, please save the app again and come back to this page.");

                console.log("Somethign wrong with this page.");

                statsUpdateService.log("ERROR : User trying to view List tab page which does not have pageoid ");

            } else {
                pageIntroOid = unescape(pageIntroOid);
                loadPageDetails(pageIntroOid);
            }



            $scope.back = function() {
                console.log(" clicked back sfsdf " + window.history);
                window.history.back();

            };

            $scope.finish = function() {
                console.log(" clicked finish ")
                $location.path('/home');
            };



            $scope.preview = function() {
                console.log(" clicked preview need to redirect to preview page");
                statsUpdateService.log("Clicked preview" + " from page : " + appPage);
                $location.path('/createPage');
            };


            $scope.viewProductDetails = function(index) {

                statsUpdateService.log("Clicked view details " + " from page : " + index + "  to oid " + $scope.listOfData[index].productDetailsoid);
                console.log("Clicked view details " + " from page : " + JSON.stringify($scope.listOfData[index]));

                $state.go("productDetailsSubPageView", {
                    pageOid: $scope.listOfData[index].productDetailsoid
                });


                // viewProductDetails


            };


            $scope.viewSubPage = function(i) {
                console.log("INDEX : : " + i);
                console.log("opened clicked on fucking next NEW : " + JSON.stringify($scope.listOfData[i]));
                /*if (($scope.listOfData[i].subPage != undefined && $scope.listOfData[i].subPage != null) && ($scope.listOfData[i].subPage.subPageType != undefined && $scope.listOfData[i].subPage.subPageType != null && $scope.listOfData[i].subPage.subPageType != "")) {
                    var subPageType = $scope.listOfData[i].subPage.subPageType;
                    var templateName = $scope.listOfData[i].subPage.subPageTemplate;
                    var redirectorName = subPageType.trim().toLowerCase() + "-" + templateName.trim();
                    var goToLink = "app." + redirectorName;
                    var dumyPoid = $scope.listOfData[i].subPage.pageOid;
                    $state.go(goToLink, {
                        pageOid: dumyPoid
                    });
                }
                */
                // go to edit full registration response create page

                var goToLink = "app.registration-WBG"
                var toBeSent = {
                    pageOid: pageIntroOid,
                    appIntroOid: $scope.listOfData[i].reponseOid
                }

                console.log("   values being sent are : " + JSON.stringify(toBeSent))
                $state.go(goToLink, toBeSent);

            }




            $scope.addAnotherItem = function() {
                var citem = {};
                citem.iHeader = "";
                var descList = [];
                var desc = {};
                desc.label = "";
                desc.value = "";
                descList.push(desc);
                citem.descList = descList;
                listOfData.push(citem);
                console.log(JSON.stringify(listOfData));
                statsUpdateService.log("Clicked addAnotherItem" + " from page : " + appPage);
            };


            $scope.removeitem = function(index) {
                console.log("index >> " + index);
                listOfData.splice(index, 1);
                console.log(JSON.stringify(listOfData));
                statsUpdateService.log("Clicked removeitem" + " from page : " + appPage);
            };




            $scope.addAnotherDesc = function(parentIndex, index) {
                var desc = {};
                desc.label = "";
                desc.value = "";
                listOfData[parentIndex].descList.push(desc);
                console.log(JSON.stringify(listOfData));
            };


            $scope.removeDesc = function(parentIndex, index) {
                console.log("index >> " + index + " parent index " + parentIndex);

                listOfData[parentIndex].descList.splice(index, 1);

                console.log(JSON.stringify(listOfData));
            };



            $scope.savePage = function() {

                toServer.listOfData = listOfData;
                appAdminService.savePageDetails(pageIntroOid, toServer);

                statsUpdateService.log("Saving Page : " + pageIntroOid + " from page : " + appPage);
                console.log(" clicked Save ")

            };


            $scope.changedDepartment = function(index) {
                // $scope.internalVars.filedInputValue
                console.log(" changedDepartment(index){ internalVars.filedInputValue : " + $scope.internalVars.filedInputValue + "  tempLOD " + tempLOD.length);

                var temp2 = [];

                if ($scope.internalVars.filedInputValue.toLowerCase() == "all" || $scope.internalVars.filedInputValue.toLowerCase().trim() == 0) {
                    $scope.listOfData = tempLOD;
                    return;
                }

                for (var i = 0; i < tempLOD.length; i++) {
                    var itemDept = getDepartmentOfItem(tempLOD[i], $scope.internalVars.filedInputValue.toLowerCase());
                    // console.log(" item status : " + itemDept);
                    if (itemDept != undefined && itemDept.toLowerCase() == $scope.internalVars.filedInputValue.toLowerCase()) {
                        temp2.push(tempLOD[i])
                    }
                }

                console.log(" POST FILTER  : " + temp2.length);

                $scope.listOfData = temp2;
            }






            function loadPageDetails(pageIntroOid) {
                var subPageJSOn = {};

                subPageJSOn.introOID = pageIntroOid;
                console.log(" pageIntroOid " + pageIntroOid)
                console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));

                var dummyResp = getCachedJson(getFullDetails + JSON.stringify(subPageJSOn));
                console.log(" dummyResp " + dummyResp)
                if (dummyResp != null) {
                    console.log(" Using Cached data ..")
                    updateDataInPage(dummyResp);
                }


                $timeout(function() {
                    if (alertPopupV != undefined) {
                        alertPopupV.close(); //close the popup after 3 seconds for some reason
                    }

                }, 3000);


                $http.post(getFullDetails, subPageJSOn).then(function successCallback(response) {
                    // console.log("Success with response : "+JSON.stringify(response));


                    var isDiffFromCache = isDataDiffFromCache(getFullDetails + JSON.stringify(subPageJSOn), response);
                    console.log(" isDiffFromCache " + isDiffFromCache)
                        // isDiffFromCache = false;

                    if (isDiffFromCache) {

                        /*alertPopupV = $ionicPopup.alert({
                            title: 'Updated data',
                            template: 'There was change in this page, and it has been updated now.'
                        });*/


                        updateDataInPage(response);
                    }

                    // updateDataInPage(response);


                }, function errorCallback(response) {
                    console.log(" response : " + JSON.stringify(response));
                    statsUpdateService.log("user callback as failed during loadListPage   : with error as :  " + JSON.stringify(response))

                });
            }


            function getDepartmentId(dataOfLOD) {
                console.log(" vals : dataOfLOD : " + JSON.stringify(dataOfLOD));


                for (var i = 0; i < dataOfLOD.descList.length; i++) {
                    console.log(" dataOfLOD.descList[i].label " + dataOfLOD.descList[i].label)
                    if (dataOfLOD.descList[i].label.toLowerCase() == "department") {
                        return i;
                    }
                }
                return null;
            }

            function updateDataInPage(response) {
                if (response.data == "error") {
                    console.log("App creator missed adding content in this page.");
                    statsUpdateService.log("ERROR : User trying to view page which does not have pageoid ");
                    return;
                }

                // response.data.listOfData = Array.prototype.slice.call(response.data.listOfData).reverse();



                console.log(" tempLOD length : " + response.data.listOfData.length)


                if (validRole("Operations")) {
                    tempLOD = listOfData = response.data.listOfData;
                    $scope.internalVars.showFilters = true;
                } else {

                    console.log(" about to filter  : before length " + response.data.listOfData.length)

                    for (var i = response.data.listOfData.length - 1; i >= 0; i--) {
                        var deptId = getDepartmentId(response.data.listOfData[i]);

                        if (deptId == undefined) {
                            // tempLOD.push(response.data.listOfData[i]);

                        } else {

                            console.log(" response.data.listOfData[i] : department  " + JSON.stringify(response.data.listOfData[i].descList[deptId].value + "   " + deptId));

                            if (validRole(response.data.listOfData[i].descList[deptId].value)) {
                                // console.log(" pushing role ");
                                // $scope.listOfData.push(response.data.listOfData[i]);
                                tempLOD.push(response.data.listOfData[i]);

                            }

                        }
                    }

                }


                console.log(" tempLOD length : " + tempLOD.length)

                tempLOD = filterLOFByCritereaa(tempLOD);
                $scope.listOfData = tempLOD;
                console.log(" RETURNED  s $scope.listOfData length : " + $scope.listOfData.length)
                toServer.pageOid = response.data._id;

            }

            // Gallery
            function getStatusOfItem(item) {


                for (var j = item.descList.length - 1; j >= 0; j--) {

                    if (item.descList[j].label != undefined && item.descList[j].label == "Status") {

                        console.log(" item.descList[j].label " + item.descList[j].label + " " + item.descList[j].value.toLowerCase());

                        if (item.descList[j].value.toLowerCase() == filterCriterea.toLowerCase()) {
                            return item.descList[j].value.toLowerCase();
                        }
                        // break;
                    }


                }


            }

            // Gallery
            function getOnlyStatusOfItem(item) {


                for (var j = item.descList.length - 1; j >= 0; j--) {

                    if (item.descList[j].label != undefined && item.descList[j].label == "Status") {

                        return item.descList[j].value;
                    }


                }


            }

            // Gallery
            function getEscallationNameOfItem(item) {


                for (var j = item.descList.length - 1; j >= 0; j--) {

                    if (item.descList[j].label != undefined && item.descList[j].label == "escalation_Nm") {

                        if (item.descList[j].value.toLowerCase() == filterCriterea.toLowerCase()) {
                            return item.descList[j].value.toLowerCase();
                        }
                        // break;
                    }


                }


            }


            // department
            function getDepartmentOfItem(item, deptName) {


                for (var j = item.descList.length - 1; j >= 0; j--) {

                    if (item.descList[j].label != undefined && item.descList[j].label.toLowerCase() == "department") {

                        if (item.descList[j].value.toLowerCase() == deptName.toLowerCase()) {
                            return item.descList[j].value.toLowerCase();
                        }
                        // break;
                    }


                }


            }




            function filterLOFByCritereaa(tempLOD) {


                var temp2 = [];

                console.log(" trying to filter#" + filterCriterea + "#")


                if (filterCriterea == '' || filterCriterea == undefined) {
                    return tempLOD;
                }

                if (filterCriterea == '') {
                    console.log("   filterCriterea IMP : " + filterCriterea)
                }

                if (filterCriterea == 'Escalation 1') {
                    return filterLODEscallation('Escalation 1', tempLOD);
                }

                if (filterCriterea == 'Escalation 2') {
                    var toBeRet = filterLODEscallation('Escalation 2', tempLOD);

                    console.log(" sc fileter len g : " + toBeRet.length)
                    return toBeRet;
                }

                var stat_id = 0;

                for (var i = 0; i < tempLOD.length; i++) {

                    var itemStatus = getStatusOfItem(tempLOD[i]);
                    // console.log(" item status : " + itemStatus);

                    if (itemStatus != undefined && itemStatus.toLowerCase() == filterCriterea.toLowerCase()) {
                        temp2.push(tempLOD[i])
                            // break;
                    }
                }


                console.log("  stat_id " + stat_id)

                currentPageDetails.filterCriterea = '';
                return temp2;

            }

            function validRole(deptName) {
                console.log(" userRoles " + userRoles);
                console.log(" deptName " + deptName);

                return (userRoles == undefined) ? true : (userRoles.indexOf(deptName) > -1);
            }



            function filterLODEscallation(escLevel_Nm, tempLOD) {
                var escLOD = [];
                console.log(" filterLODEscallation " + tempLOD.length + " escLevel_Nm " + escLevel_Nm)
                for (var i = 0; i < tempLOD.length; i++) {
                    var item = tempLOD[i];


                    var itemStatus = getOnlyStatusOfItem(item);

                    if (item.escalation_Nm != undefined || item.escalation_Nm != null) {
                        // console.log("  escalation detected.. item.escalation_Nm :" + item.escalation_Nm + "  escLevel_Nm " + escLevel_Nm + " itemStatus" + itemStatus + "  iHeader " + item.iHeader)
                        // console.log(" itemESC :  " + JSON.stringify(item))

                        if (itemStatus != undefined && escLevel_Nm == item.escalation_Nm && itemStatus.toLowerCase() != 'close') {
                            escLOD.push(item)
                        }

                    }

                }
                return escLOD;
            }

            $scope.doRefresh = function() {
                    $scope.times = 0;
                    $scope.items = [];
                    $scope.postsCompleted = false;
                    $scope.getPosts();
                    $scope.$broadcast('scroll.refreshComplete');
                }
                // modal to show image full screen
            $ionicModal.fromTemplateUrl('screens/list-thumbnails5050_WBG/gallery-modal.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.modal = modal;
            });
            $scope.openModal = function() {
                $scope.showNav = true;
                $scope.modal.show();
            };

            $scope.closeModal = function() {
                $scope.modal.hide();
            };
            // show image in popup
            $scope.showImage = function(x) {
                    $scope.imageIndex = x;
                    $scope.imageSrc = $scope.listOfData[x].iconsImages[0].imageURL;
                    $scope.openModal();
                }
                // image navigation // swiping and buttons will also work here
            $scope.imageNavigate = function(dir) {
                    if (dir == 'right') {
                        $scope.imageIndex = $scope.imageIndex + 1;
                    } else {
                        $scope.imageIndex = $scope.imageIndex - 1;
                    }

                    // alert($scope.listOfData[$scope.imageIndex]);

                    if ($scope.listOfData[$scope.imageIndex] === undefined) {
                        // $scope.closeModal();
                        $scope.imageIndex = 0;
                        $scope.imageSrc = $scope.listOfData[$scope.imageIndex].iconsImages[0].imageURL;
                    } else {
                        $scope.imageSrc = $scope.listOfData[$scope.imageIndex].iconsImages[0].imageURL;

                    }
                }
                // cleaning modal
                /*            $scope.$on('$stateChangeStart', function() {
                                $scope.modal.remove();
                            });*/
                // gallery END

        }
    ]);
