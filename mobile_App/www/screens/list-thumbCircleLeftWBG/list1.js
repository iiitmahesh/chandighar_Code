'use strict';

angular.module('myApp.list', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
    $routeProvider.when('/list/:pageOid', {
        templateUrl: 'screens/list-thumbCircleLeftWBG/list.html',
        controller: 'listCtrl'
    });
}])

.controller(
    'listCtrl', [
        '$scope',
        '$state',
        '$location',
        '$stateParams',
        '$http',
        '$routeParams',
        'appAdminService',
        'statsUpdateService',
        function($scope, $state, $location, $stateParams, $http, $routeParams, appAdminService, statsUpdateService) {

            // var pageIntroOid = $routeParams.pageOid;
            var pageIntroOid = $stateParams.pageOid;
            console.log(" pageIntroOid in view " + pageIntroOid);

            var getFullDetails = centralServer + '/getFullDetails';
            $scope.pageSpecificData.centralServer = centralServer;

            var appPage = "list";
            var listOfData = [];
            var citem = {};
            citem.iHeader = "";

            var descList = [];
            var desc = {};
            desc.label = "";
            desc.value = "";
            descList.push(desc);
            citem.descList = descList;
            listOfData.push(citem);

            $scope.listOfData = listOfData;
            var toServer = {};



            if ("notYetSaved" == pageIntroOid) {
                // alert(" There is some error with this page, please save the app again and come back to this page.");

                window.alert("Somethign wrong with this page.");

                statsUpdateService.log("ERROR : User trying to view List tab page which does not have pageoid ");

            } else {
                pageIntroOid = unescape(pageIntroOid);
                loadPageDetails(pageIntroOid);
            }



            $scope.back = function() {
                console.log(" clicked back sfsdf " + window.history);
                window.history.back();

            };

            $scope.finish = function() {
                console.log(" clicked finish ")
                $location.path('/home');
            };



            $scope.preview = function() {
                console.log(" clicked preview need to redirect to preview page");
                statsUpdateService.log("Clicked preview" + " from page : " + appPage);
                $location.path('/createPage');
            };


            $scope.viewProductDetails = function(index) {

                statsUpdateService.log("Clicked view details " + " from page : " + index + "  to oid " + $scope.listOfData[index].productDetailsoid);
                console.log("Clicked view details " + " from page : " + JSON.stringify($scope.listOfData[index]));

                $state.go("productDetailsSubPageView", {
                    pageOid: $scope.listOfData[index].productDetailsoid
                });


                // viewProductDetails


            };


            $scope.viewSubPage = function(i) {
                console.log("opened clicked on fucking next" + JSON.stringify($scope.listOfData[i].subPage));
                if (($scope.listOfData[i].subPage != undefined && $scope.listOfData[i].subPage != null) && ($scope.listOfData[i].subPage.subPageType != undefined && $scope.listOfData[i].subPage.subPageType != null && $scope.listOfData[i].subPage.subPageType != "")) {

                    var subPageType = $scope.listOfData[i].subPage.subPageType;
                    var goToLink = "app." + subPageType.trim().toLowerCase();
                    var dumyPoid = $scope.listOfData[i].subPage.pageOid;
                    $state.go(goToLink, {
                        pageOid: dumyPoid
                    });
                }
            }




            $scope.addAnotherItem = function() {
                var citem = {};
                citem.iHeader = "";
                var descList = [];
                var desc = {};
                desc.label = "";
                desc.value = "";
                descList.push(desc);
                citem.descList = descList;
                listOfData.push(citem);
                console.log(JSON.stringify(listOfData));
                statsUpdateService.log("Clicked addAnotherItem" + " from page : " + appPage);
            };


            $scope.removeitem = function(index) {
                console.log("index >> " + index);
                listOfData.splice(index, 1);
                console.log(JSON.stringify(listOfData));
                statsUpdateService.log("Clicked removeitem" + " from page : " + appPage);
            };




            $scope.addAnotherDesc = function(parentIndex, index) {
                var desc = {};
                desc.label = "";
                desc.value = "";
                listOfData[parentIndex].descList.push(desc);
                console.log(JSON.stringify(listOfData));
            };


            $scope.removeDesc = function(parentIndex, index) {
                console.log("index >> " + index + " parent index " + parentIndex);

                listOfData[parentIndex].descList.splice(index, 1);

                console.log(JSON.stringify(listOfData));
            };



            $scope.savePage = function() {

                toServer.listOfData = listOfData;
                appAdminService.savePageDetails(pageIntroOid, toServer);

                statsUpdateService.log("Saving Page : " + pageIntroOid + " from page : " + appPage);
                console.log(" clicked Save ")

            };





            function loadPageDetails(pageIntroOid) {
                var subPageJSOn = {};

                subPageJSOn.introOID = pageIntroOid;
                console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));

                $http.post(getFullDetails, subPageJSOn).then(function successCallback(response) {
                    console.log("Success with response : " + JSON.stringify(response.data));
                    if (response.data == "error") {
                        // alert("There is no data available.");
                        window.alert("App creator missed adding content in this page.");

                        statsUpdateService.log("ERROR : User trying to view page which does not have pageoid ");

                        return;
                    }

                    $scope.listOfData = listOfData = response.data.listOfData;
                    toServer.pageOid = response.data._id;

                }, function errorCallback(response) {
                    console.log(" response : " + JSON.stringify(response));
                    statsUpdateService.log("user callback as failed during loadListPage   : with error as :  " + JSON.stringify(response))

                });
            }



        }
    ]);
