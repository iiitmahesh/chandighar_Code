'use strict';

angular.module('myApp.home-rowListFull', ['ngRoute'])
    // .config(['$routeProvider', function($routeProvider) {
    //     $routeProvider.when('/home-rowListFull', {
    //         templateUrl: 'screens/home-rowListFull/home.html',
    //         controller: 'homeCtrl'
    //     });
    // }])
    .controller(
        'homeCtrl', [
            '$scope',
            '$state',
            '$location',
            '$http',
            '$routeParams',
            '$stateParams',

            '$interval',
            'appAdminService',
            'statsUpdateService',
            function($scope, $state, $location, $http, $routeParams, $stateParams, $interval, appAdminService, statsUpdateService) {

                // alert(" in Home page");

                var appPage = "viewPage";



                var pageSpecificData = {};
                pageSpecificData.centralServer = centralServer;
                pageSpecificData.showSplash = true;
                $scope.pageSpecificData.centralServer = centralServer;

                var categories = [];
                var citem = {};
                citem.subPageName = "";
                citem.subPageType = "";
                citem.pageOid = "";
                $scope.showPagData = {
                    appName: "",
                    description: "",
                    themeName: ""
                };


                categories.push(citem);
                //                        $scope.categories = categories;
                var pageDataToserver = {},
                    data = {};
                // var pageIntroOid = $routeParams.pageOid;
                // var pageIntroOid = $stateParams.pageOid;
                var pageIntroOid = globalPageIntroOid;

                var getFullDetails = centralServer + '/getIntro';

                console.log(" pageIntroOid " + pageIntroOid);



                loadPageDetails(pageIntroOid);


                /*
                                if ("notYetSaved" == pageIntroOid) {
                                    //                    alert(" There is some error with this page, please save the app again and come back to this page.")
                                } else {
                                    pageIntroOid = unescape(pageIntroOid);
                                    loadPageDetails(pageIntroOid);
                                }
                */
                function loadPageDetails(pageIntroOid) {
                    // alert("insiude  loadPageDetails " + pageIntroOid);

                    var subPageJSOn = {};

                    statsUpdateService.log("came To ViewPage with pageIntroOid as --- " + pageIntroOid);

                    subPageJSOn.introOID = pageIntroOid;
                    console.log(" subPageJSOn " + JSON.stringify(subPageJSOn));




                    $http.post(getFullDetails, subPageJSOn).then(function successCallback(response) {
                        console.log("Success with response : " + JSON.stringify(response.data));
                        // alert(" response.data " + JSON.stringify(response.data))

                        if (response.data == "error") {
                            alert(" Sorry some internal error has occured. ");
                            statsUpdateService.log("loadPageDetails Error...");
                            return;
                        }

                        // console.log(" response.data > of subpage " + JSON.stringify(response.data));


                        pageDataToserver = response.data;
                        $scope.showPagData.appName = pageDataToserver.appName;
                        $scope.showPagData.themeName = pageDataToserver.themeName;
                        if (pageDataToserver.splashImage == "" || pageDataToserver.splashImage == undefined) {
                            $scope.pageSpecificData.imageSrc = 'img/left.png'
                        } else {
                            console.log(centralServer + pageDataToserver.splashImage);
                            $scope.pageSpecificData.imageSrc = centralServer + pageDataToserver.splashImage;
                        }


                        // $scope.$apply();

                        $scope.showPagData.description = pageDataToserver.description;

                        if (pageDataToserver.categories != null) {

                            var internalCatsRec = pageDataToserver.categories;
                            var internalCats = [];


                            // console.log(" internalCatsRec > " + JSON.stringify(internalCatsRec));
                            for (var i = 0; i < internalCatsRec.length; i++) {
                                var cat = internalCatsRec[i];
                                if (!((cat.subPageName == "") || (cat.subPageType == "") || (cat.pageOid == ""))) {
                                    internalCats.push(cat);
                                }

                            }

                            console.log("InternalCats final >> " + JSON.stringify(internalCats));

                            categories = pageDataToserver.categories;
                            $scope.categories = internalCats;
                            console.log(" $scope.categories " + JSON.stringify($scope.categories))

                        }
                    }, function errorCallback(response) {

                        //alert(" response.data " + JSON.stringify(response))

                        console.log(" response : " + JSON.stringify(response));
                        statsUpdateService.log("loadPageDetails Error....");
                    });
                }



                $scope.home = function() {
                    console.log(" clicked goToHome ");
                    statsUpdateService.log("Going to Home from page : " + appPage);
                    $location.path('/home');
                };


                $scope.back = function() {
                    console.log(" clicked finish ");
                    currentApp = null;
                    statsUpdateService.log("Going to prev page from Page : " + appPage);
                    window.history.back();

                    //                    $location.path('/home');
                };


                $scope.viewSubPage = function(index, filterCriterea) {
                    console.log(" to be taken to based on " + JSON.stringify($scope.categories[index]));

                    statsUpdateService.log("Viewing subpage from function :  viewSubPage from file : " + appPage);

                    var subPageType = $scope.categories[index].subPageType;


                    var templateName = $scope.categories[index].subPageTemplate;
                    var redirectorName = subPageType.trim().toLowerCase() + "-" + templateName.trim();
                    var goToLink = "app." + redirectorName;

                    // var goToLink = subPageType.trim().toLowerCase() + "SubPageView";
                    // var goToLink = "app." + subPageType.trim().toLowerCase();

                    if (subPageType == "" || subPageType == null || subPageType == undefined) {
                        alert(" Tab not yet completely designed.");
                        return;
                    }

                    var dumyPoid = $scope.categories[index].pageOid;
                    if (dumyPoid != null) {
                        // goToLink = goToLink + '/' + escape(dumyPoid);
                        console.log("new to link app." + goToLink + dumyPoid)

                        currentPageDetails.filterCriterea = filterCriterea;

                        $state.go(goToLink, {
                            pageOid: dumyPoid,
                            filterCriterea: filterCriterea
                        }, { reload: true });


                    } else {
                        //                        goToLink = goToLink + '/notYetSaved';
                        alert("Either page did not get saved properly or Tab no longer exists.");
                        statsUpdateService.log("Either page did not get saved properly or Tab no longer exists from page : " + appPage);
                    }
                    // console.log(" goToLink :  " + goToLink);
                    // $location.path(goToLink);


                };


                $scope.generateAPK = function() {
                    $scope.downloadingMesssage = "Building your apk, please be patient. It might take about 2 minutes to complete the process."
                    var respHandle = appAdminService.generateAPK($scope.displayPageData);
                    handleAPKDownload(respHandle);
                }


                function handleAPKDownload(respHandle) {

                    respHandle.then(function successCallback(response) {
                        console.log(" download compelted .. ");
                        // $scope.downloadingMesssage = "";

                        var a = document.createElement('a');
                        a.href = 'data:attachment/csv;charset=utf-8,' + encodeURI(response);
                        a.target = '_blank';
                        a.download = 'debug.apk';
                        document.body.appendChild(a);
                        a.click();


                    }, function errorCallback(response) {
                        // $scope.downloadingMesssage = "";

                        alert("Sorry some internal occured. Please notify admin to correct the applciation.");
                        console.log(" response : " + JSON.stringify(response));
                        statsUpdateService.log(" ajax call failed at create app page - > get registration responses : " + JSON.stringify(response));

                    });

                }





            }
        ]);
