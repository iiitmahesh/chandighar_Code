'use strict';

var getIntro = centralServer+'/getIntro';
var getFullDetails = centralServer+'/getFullDetails';
var createUserURL = centralServer+'/createUser'


angular.module('gamifad.services',[])

.factory('gamifadServices',function($http) {
	return{
    getIntro : function(inputJson){
      return $http({
        method : 'POST',
        url : getIntro,
        data : inputJson
      });

    },
    
    getFulldetails : function(inputJson){
      return $http({
        method : 'POST',
        url : getFullDetails,
        data : inputJson
      });

    },

    register : function(user) {

        $http.post(createUserURL, user).then(function successCallback(response) {
            console.log("Success with response : " + JSON.stringify(response.data));
            if (response.data == "error") {
                alert(" Sorry some internal error has occured. ")
                return;
            }
            userINTROOID = user.rid = response.data;
            saveData(userINTROOID_KEY, user.rid);
            //$state.go("app.home");

        }, function errorCallback(response) {

        });
    }

}

})