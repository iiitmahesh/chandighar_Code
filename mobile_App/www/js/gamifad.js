'use strict';

angular.module('gamifad', [])

.controller('pageCtrl', function(gamifadServices, statsUpdateService, $scope, $state, $stateParams, $ionicModal, $rootScope) {

     $rootScope.centralServer = centralServer;

   $scope.viewSubPage = function(x,y,z) {
        // x,y,z = templateType,templateName,pageOid
        //ex x,y,z = list,thumb-5050TBG,#12:88
        // var y= "thumb5050TBG";
        console.log("app."+x+"-"+y);
        statsUpdateService.log("going to page with id:  " + z);
        $state.go("app."+x+"-"+y, {
            pid: z
        });
    }


     $ionicModal.fromTemplateUrl('templates/core/modal.html', function ($ionicModal ) {
            $scope.modal = $ionicModal;
        }, {
            scope: $scope,
            animation: 'slide-in-up'
        });

    $scope.itemFullView = function()
    {
        // $scope.currentItem = x;
        // console.log(JSON.stringify(x));
        $scope.modal.show();
    }

     $scope.closeView = function() {
            $scope.modal.hide();
          };
          

})






// appAdminService, statsUpdateService
